# Unity Standard Assets

对资源[Standard Assets](https://www.assetstore.unity3d.com/#!/content/32351)打包，兼容Unity2018资源包管理系统

# 使用方式

克隆或下载解压至工程目录Packages文件夹下即可，无需修改其他文件。

Packages下文件夹可修改名称。